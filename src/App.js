import React from 'react';
import Navbar from './components/Navbar/Navbar'
import TopBar from './components/TopBar/TopBar'
import Dashboard from './components/Dashboard/Dashboard'
import Pedidos from './components/Pedidos/Pedidos'
import { Grid } from 'semantic-ui-react'
import { BrowserRouter as Router, Route } from 'react-router-dom';

function App() {
  return (
    <Grid className="App">
      <Grid.Row className="row1">
        <Grid.Column>
          <TopBar />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row className="row2">
        <Grid.Column width={3} className="menuContainer">
          <Navbar />
        </Grid.Column>
        <Grid.Column width={13} className="infoContainer">
          <Router>
              <Route exact path="/" component={Dashboard} />
              <Route exact path="/pedidos" component={Pedidos} />
              {/*<Route exact path="/juegos" component={PaginaJuegos} />
  <Route path="/jugar" component={Juego} />*/}
          </Router>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}

export default App;
