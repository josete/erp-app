import React from 'react';
import { Grid, Header} from 'semantic-ui-react';
import $ from "jquery";

class TopBar extends React.Component {
    state = {total:0,sent:0,nosent:0}    
    getOrdersInfo(){
        var info = $.ajax({
            url: "http://localhost:3001/orders/month",
            type: "GET",
            crossDomain: true,
            async: false
        }).responseText;
        var infoJson = JSON.parse(info);
        this.setState({total:infoJson.total,sent:infoJson.sent,nosent:infoJson.nosent});
    }
    componentWillMount(){
        this.getOrdersInfo();
    }
    render(){               
        return(<Grid columns='equal' id="topbar">
        <Grid.Column>
            <Header as="h3">Empresa</Header>
        </Grid.Column>
        <Grid.Column>
            <Header sub>Pedidos este mes</Header>
            <span>{this.state.total}</span>
        </Grid.Column>
        <Grid.Column>
            <Header sub>Pedidos enviados</Header>
            <span>{this.state.sent}</span>
        </Grid.Column>
        <Grid.Column>
            <Header sub>Pedidos sin enviar</Header>
            <span>{this.state.nosent}</span>
        </Grid.Column>
        <Grid.Column>
            <Header sub>Visitas este mes</Header>
            <span>300</span>
        </Grid.Column>
        <Grid.Column>
            <Header sub>08/03/2020</Header>
            <Header sub>13:00</Header>
        </Grid.Column>
      </Grid>)
    }
}

export default TopBar;