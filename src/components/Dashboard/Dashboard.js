import React from 'react';
import { Grid, Segment } from 'semantic-ui-react'
import { ResponsiveLine } from '@nivo/line'
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import $ from "jquery";


class Dashboard extends React.Component {

    state = { ordersData: null, positionsMarkers: null }
    getOrdersDataGraph() {
        var info = $.ajax({
            url: "http://localhost:3001/orders/graph",
            type: "GET",
            crossDomain: true,
            async: false
        }).responseText;
        var infoJson = JSON.parse(info);
        var dataArray = [];
        var data2Array = [];
        for (var i = 0; i < infoJson.ordersGraph.length; i++) {
            data2Array.push({ x: infoJson.ordersGraph[i].month, y: infoJson.ordersGraph[i].total });
        }
        dataArray.push({ data: data2Array });
        this.setState({ ordersData: dataArray });
    }
    getPositionMarkers() {
        var info = $.ajax({
            url: "http://localhost:3001/orders/map",
            type: "GET",
            crossDomain: true,
            async: false
        }).responseText;
        var infoJson = JSON.parse(info);
        this.setState({ positionsMarkers: infoJson.ordersMap });
    }
    getMinValue(data) {
        var min;
        for (var i = 0; i < data[0].data.length; i++) {
            if (min === undefined) {
                min = data[0].data[i].y;
            } else {
                if (min > data[0].data[i].y) {
                    min = data[0].data[i].y;
                }
            }
        }
        return min;
    }
    componentWillMount() {
        this.getOrdersDataGraph();
        this.getPositionMarkers();
    }
    render() {
        const data2 = [
            {
                "color": "hsl(191, 70%, 50%)",
                "data": [
                    {
                        "x": "Enero",
                        "y": 5
                    },
                    {
                        "x": "Febrero",
                        "y": 10
                    },
                    {
                        "x": "Marzo",
                        "y": 12
                    },
                    {
                        "x": "Abril",
                        "y": 20
                    }
                ]
            }
        ];
        const position = [0, 0];
        var icon = new L.Icon.Default();
        icon.options.shadowSize = [0,0];
        return (<Grid columns={2} className="dashboard">
            <Grid.Row stretched>
                <Grid.Column>
                    <Segment className="chartContainer">
                        <h4>Pedidos mensuales</h4>
                        <ResponsiveLine
                            data={this.state.ordersData}
                            margin={{ top: 10, right: 60, bottom: 60, left: 60 }}
                            xScale={{ type: 'point' }}
                            yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                orient: 'bottom',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Mes',
                                legendOffset: 36,
                                legendPosition: 'middle'
                            }}
                            axisLeft={{
                                orient: 'left',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Pedidos',
                                legendOffset: -40,
                                legendPosition: 'middle'
                            }}
                            colors={{ scheme: 'dark2' }}
                            enableArea={true}
                            areaBaselineValue={this.getMinValue(this.state.ordersData)}
                            pointSize={10}
                            pointColor={{ theme: 'background' }}
                            pointBorderWidth={2}
                            pointBorderColor={{ from: 'serieColor' }}
                            pointLabel="Pedidos"
                            pointLabelYOffset={-12}
                            useMesh={true}
                        />
                    </Segment>
                    <Segment className="chartContainer">
                        <h4>Visitas mensuales</h4>
                        <ResponsiveLine
                            data={data2}
                            margin={{ top: 10, right: 60, bottom: 60, left: 60 }}
                            xScale={{ type: 'point' }}
                            yScale={{ type: 'linear', min: 'auto', max: 'auto', stacked: true, reverse: false }}
                            axisTop={null}
                            axisRight={null}
                            axisBottom={{
                                orient: 'bottom',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Mes',
                                legendOffset: 36,
                                legendPosition: 'middle'
                            }}
                            axisLeft={{
                                orient: 'left',
                                tickSize: 5,
                                tickPadding: 5,
                                tickRotation: 0,
                                legend: 'Visitas',
                                legendOffset: -40,
                                legendPosition: 'middle'
                            }}
                            colors={{ scheme: 'dark2' }}
                            enableArea={true}
                            areaBaselineValue={this.getMinValue(data2)}
                            pointSize={10}
                            pointColor={{ theme: 'background' }}
                            pointBorderWidth={2}
                            pointBorderColor={{ from: 'serieColor' }}
                            pointLabel="Pedidos"
                            pointLabelYOffset={-12}
                            useMesh={true}
                        />
                    </Segment>
                </Grid.Column>
                <Grid.Column>
                    <Segment>
                        <h4>Mapa de Pedidos</h4>
                        <Map center={position} zoom={1} className="mapContainer">
                            <TileLayer
                                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                                attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                            />
                            {this.state.positionsMarkers.map(function (marker) {
                                return <Marker position={[marker.lat,marker.lon]} icon={icon}>
                                    <Popup><b>{marker.pc}</b><br/>Pedidos:{marker.count}</Popup>
                                </Marker>

                            })}
                        </Map>
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        </Grid>);
    }

}

export default Dashboard;