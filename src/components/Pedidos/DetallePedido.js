import React from 'react';
import { Modal, Table, Icon, TableBody, Button } from 'semantic-ui-react'

class DetallePedido extends React.Component {

    render() {
        return (
            <Modal open={this.props.open} onClose={this.props.close} closeIcon closeOnDimmerClick>
                <Modal.Header>Pedido: {this.props.orderid}</Modal.Header>
                <Modal.Content>
                    <Table celled structured unstackable>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4'>Nombre</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell colSpan='4'>{this.props.nombre}</Table.Cell>
                            </Table.Row>
                        </TableBody>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4'>Direccion</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell colSpan='4'>{this.props.address}</Table.Cell>
                            </Table.Row>
                        </TableBody>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Ciudad</Table.HeaderCell>
                                <Table.HeaderCell>Pronvincia</Table.HeaderCell>
                                <Table.HeaderCell>CP</Table.HeaderCell>
                                <Table.HeaderCell>Pais</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell>{this.props.city}</Table.Cell>
                                <Table.Cell>{this.props.province}</Table.Cell>
                                <Table.Cell>{this.props.pc}</Table.Cell>
                                <Table.Cell>{this.props.country}</Table.Cell>
                            </Table.Row>
                        </TableBody>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='2'>Telefono</Table.HeaderCell>
                                <Table.HeaderCell colSpan='2'>Email</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell colSpan='2'>{this.props.tel}</Table.Cell>
                                <Table.Cell colSpan='2'>{this.props.email}</Table.Cell>
                            </Table.Row>
                        </TableBody>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='4'>Productos</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            {this.props.products.map(function (product) {
                                return (<Table.Row>
                                    <Table.Cell colSpan='2'>{product.name}</Table.Cell>
                                    <Table.Cell colSpan='2'>{product.qty}</Table.Cell>
                                </Table.Row>)
                            })}
                        </TableBody>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell colSpan='2'>Fecha</Table.HeaderCell>
                                <Table.HeaderCell colSpan='2'>Estado</Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <TableBody>
                            <Table.Row>
                                <Table.Cell colSpan='2'>{new Date(this.props.date).getDate()}-{new Date(this.props.date).getMonth() + 1}-{new Date(this.props.date).getFullYear()}</Table.Cell>
                                <Table.Cell colSpan='2'>{this.props.state ? "Enviado" : "Sin enviar"}</Table.Cell>
                            </Table.Row>
                        </TableBody>
                        {/*<Table.Body>
                            <Table.Row>
                                <Table.Cell colSpan='4'><Icon name='user' />{this.props.nombre}</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell colSpan='4'><Icon name='home' />{this.props.address}</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell>{this.props.city}</Table.Cell>
                                <Table.Cell>{this.props.province}</Table.Cell>
                                <Table.Cell>{this.props.pc}</Table.Cell>
                                <Table.Cell>{this.props.country}</Table.Cell>
                            </Table.Row>
                            <Table.Row>
                                <Table.Cell colSpan='2'><Icon name='phone' />{this.props.tel}</Table.Cell>
                                <Table.Cell colSpan='2'><Icon name='mail' />{this.props.email}</Table.Cell>
                            </Table.Row>
                        </Table.Body>*/}
                    </Table>
                </Modal.Content>
                <Modal.Actions>
                    <Button positive>Enviar Pedido</Button>
                    <Button negative>Cancelar Pedido</Button>                    
                </Modal.Actions>
            </Modal>);
    }

}

export default DetallePedido;