import React from 'react';
import { Table, Icon } from 'semantic-ui-react'

class PedidoFila extends React.Component {

    onClick(name){
        console.log(name);
    }
    render() {
        /*{this.state.orders.map(function (order) {
                        return(<PedidoFila orderid={order.orderid} name={order.name} surname={order.surname} 
                            city={order.city} date={order.date} sent={order.sent}/>)
                    })}*/
        return (
            <Table.Row>
                <Table.Cell selectable><a href="#" name={this.props.orderid} onClick={this.onClick(this.props.orderid)}>{this.props.orderid}</a></Table.Cell>
                <Table.Cell>{this.props.name} {this.props.surname}</Table.Cell>
                <Table.Cell>{this.props.city}</Table.Cell>
                <Table.Cell>{new Date(this.props.date).getDate()}-{new Date(this.props.date).getMonth() + 1}-{new Date(this.props.date).getFullYear()}</Table.Cell>
                <Table.Cell>{this.props.sent ? <Icon color='green' name='checkmark' /> : <Icon color='red' name='close' />}</Table.Cell>
            </Table.Row>
        );
    }
}

export default PedidoFila;