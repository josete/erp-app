import React, { Fragment } from 'react';
import { Table, Icon } from 'semantic-ui-react'
import $ from 'jquery';
import DetallePedido from './DetallePedido';

class Pedidos extends React.Component {
    state = { orders: null }
    openOrder(e) {
        var statevar = "open-" + e.target.name;
        this.setState({ [statevar]: true });
    }
    closeOrder(e, id) {
        this.setState({ [id]: false });
    }
    getOrdersData() {
        var info = $.ajax({
            url: "http://localhost:3001/orders",
            type: "GET",
            crossDomain: true,
            async: false
        }).responseText;
        var infoJson = JSON.parse(info);
        console.log(infoJson.orders);
        this.setState({ orders: infoJson.orders });
    }
    componentWillMount() {
        this.getOrdersData();
    }
    render() {
        return (
            <Table celled selectable textAlign={"center"} className="orderTable">
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Id Pedido</Table.HeaderCell>
                        <Table.HeaderCell>Nombre</Table.HeaderCell>
                        <Table.HeaderCell>Ciudad</Table.HeaderCell>
                        <Table.HeaderCell>Fecha</Table.HeaderCell>
                        <Table.HeaderCell>Enviado</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.state.orders.map(function (order) {
                        return <Fragment>
                            <Table.Row>
                                <Table.Cell selectable><a href="#" name={order.orderid} onClick={this.openOrder.bind(this)}>{order.orderid}</a></Table.Cell>
                                <Table.Cell>{order.name} {order.surname}</Table.Cell>
                                <Table.Cell>{order.city}</Table.Cell>
                                <Table.Cell>{new Date(order.date).getDate()}-{new Date(order.date).getMonth() + 1}-{new Date(order.date).getFullYear()}</Table.Cell>
                                <Table.Cell>{order.sent ? <Icon color='green' name='checkmark' /> : <Icon color='red' name='close' />}</Table.Cell>
                            </Table.Row>
                            <DetallePedido open={this.state["open-" + order.orderid]} close={((e) => this.closeOrder(e, "open-" + order.orderid))}
                            orderid={order.orderid} nombre={order.name+" "+order.surname} address={order.address} city={order.city}
                            province={order.province} pc={order.pc} country={order.country} tel={order.tel} email={order.email}
                            date={order.date} state={order.sent} products={order.products}/>
                        </Fragment>
                    }, this)}
                </Table.Body>
            </Table>
        );
    }
}

export default Pedidos;