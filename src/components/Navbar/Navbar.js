import React from 'react';
import { Icon, Menu} from 'semantic-ui-react'

class Navbar extends React.Component {
    state = {}
    handleItemClick = (e, { name }) => {
      switch(name){
        case "pedidos":
          var url = "http://" + window.location.host+"/pedidos";
          window.location = url;
          break;
      }
      this.setState({ activeItem: name })
    }
    render() {
        const { activeItem } = this.state
        return (
                <Menu vertical pointing secondary className="navbar">
        <Menu.Item
          name='pedidos'
          active={activeItem === 'pedidos'}
          onClick={this.handleItemClick}>
          Pedidos
          <Icon name='clipboard list' />
        </Menu.Item>
        <Menu.Item
          name='almacen'
          active={activeItem === 'almacen'}
          onClick={this.handleItemClick}>
          Almacen
          <Icon name='warehouse' />
        </Menu.Item>
        <Menu.Item
          name='clientes'
          active={activeItem === 'clientes'}
          onClick={this.handleItemClick}>
          Clientes
          <Icon name='address book'/>
        </Menu.Item>
        <Menu.Item
          name='web'
          active={activeItem === 'web'}
          onClick={this.handleItemClick}>
          Web
          <Icon name='server'/>
        </Menu.Item>
        <Menu.Item
          name='marketing'
          active={activeItem === 'marketing'}
          onClick={this.handleItemClick}>
          Marketing
          <Icon name='bullhorn'/>
        </Menu.Item>
        <Menu.Item
          name='documentos'
          active={activeItem === 'documentos'}
          onClick={this.handleItemClick}>
          Documentos
          <Icon name='file'/>
        </Menu.Item>
        </Menu>
        )
    }
}
export default Navbar;